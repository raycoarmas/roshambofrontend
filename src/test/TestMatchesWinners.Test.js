import React from 'react';
import { rest } from 'msw'
import { setupServer } from 'msw/node'
import { render } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect'
import TotalMatchesWinners from '../components/TotalMatchesWinners';
import { ROSHAMBO_URL, ROSHAMBO_TOTAL_MATCHES_WINNERS_ENDPOINT, } from '../constants/configConstants';

var jsonData = {
    playerOneWins: 0,
    playerTwoWins: 0,
    draws: 0,
    totalMatches: 0
}

const server = setupServer(
    rest.get(`${ROSHAMBO_URL}${ROSHAMBO_TOTAL_MATCHES_WINNERS_ENDPOINT}`, (req, res, ctx) => {
        return res(ctx.json(jsonData))
    }),
)

beforeAll(() => server.listen())
afterEach(() => server.resetHandlers())
afterAll(() => server.close())

test('renders', async () => {
    const { findByText } = render(<TotalMatchesWinners />);
    const totalRound = await findByText(/Total rounds played: 0/i);
    const playerOneWins = await findByText(/Player one: 0 wins/i);
    const playerTwoWins = await findByText(/Player two: 0 wins/i);
    const draws = await findByText(/Draws: 0/i);

    expect(totalRound).toBeInTheDocument();
    expect(playerOneWins).toBeInTheDocument();
    expect(playerTwoWins).toBeInTheDocument();
    expect(draws).toBeInTheDocument();
});

test('PLayer one win', async () => {
    jsonData = {
        playerOneWins: 1,
        playerTwoWins: 0,
        draws: 0,
        totalMatches: 1
    }
    const { findByText } = render(<TotalMatchesWinners />);
    const totalRound = await findByText(/Total rounds played: 1/i);
    const playerOneWins = await findByText(/Player one: 1 wins/i);
    const playerTwoWins = await findByText(/Player two: 0 wins/i);
    const draws = await findByText(/Draws: 0/i);

    expect(totalRound).toBeInTheDocument();
    expect(playerOneWins).toBeInTheDocument();
    expect(playerTwoWins).toBeInTheDocument();
    expect(draws).toBeInTheDocument();
});

test('player two win', async () => {
    jsonData = {
        playerOneWins: 0,
        playerTwoWins: 1,
        draws: 0,
        totalMatches: 1
    }
    const { findByText } = render(<TotalMatchesWinners />);
    const totalRound = await findByText(/Total rounds played: 1/i);
    const playerOneWins = await findByText(/Player one: 0 wins/i);
    const playerTwoWins = await findByText(/Player two: 1 wins/i);
    const draws = await findByText(/Draws: 0/i);

    expect(totalRound).toBeInTheDocument();
    expect(playerOneWins).toBeInTheDocument();
    expect(playerTwoWins).toBeInTheDocument();
    expect(draws).toBeInTheDocument();

});

test('draw', async () => {
    jsonData = {
        playerOneWins: 0,
        playerTwoWins: 0,
        draws: 1,
        totalMatches: 1
    }
    const { findByText } = render(<TotalMatchesWinners />);
    const totalRound = await findByText(/Total rounds played: 1/i);
    const playerOneWins = await findByText(/Player one: 0 wins/i);
    const playerTwoWins = await findByText(/Player two: 0 wins/i);
    const draws = await findByText(/Draws: 1/i);

    expect(totalRound).toBeInTheDocument();
    expect(playerOneWins).toBeInTheDocument();
    expect(playerTwoWins).toBeInTheDocument();
    expect(draws).toBeInTheDocument();
});
