import React from 'react';
import { rest } from 'msw'
import { setupServer } from 'msw/node'
import { render, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect'
import RoshamboMatch from '../components/RoshamboMatch';
import { ROSHAMBO_URL, ROSHAMBO_PLAY_MATCH_ENDPOINT, ROSHAMBO_HISTORY_ENDPOINT, ROSHAMBO_DELETE_HISTORY_ENDPOINT } from '../constants/configConstants';


const server = setupServer(
  rest.get(`${ROSHAMBO_URL}${ROSHAMBO_PLAY_MATCH_ENDPOINT}`, (req, res, ctx) => {
    return res(ctx.json({
      playerOneChoose: "Paper",
      playerTwoChoose: "Rock",
      playerWinner: "Player 1 win"
    }))
  }),
  rest.get(`${ROSHAMBO_URL}${ROSHAMBO_HISTORY_ENDPOINT}`, (req, res, ctx) => {
    return res(ctx.json([{
      playerOneChoose: "Paper",
      playerTwoChoose: "Rock",
      playerWinner: "Player 1 win"
    },{
      playerOneChoose: "Scissors",
      playerTwoChoose: "Rock",
      playerWinner: "Player 2 win"
    },{
      playerOneChoose: "Rock",
      playerTwoChoose: "Rock",
      playerWinner: "Draw"
    }]))
  }),
  rest.delete( `${ROSHAMBO_URL}${ROSHAMBO_DELETE_HISTORY_ENDPOINT}`, (req, res, ctx) => {
    return res()
  }),
 
)

beforeAll(() => server.listen())
afterEach(() => server.resetHandlers())
afterAll(() => server.close())

test('renders data', async () => {
  const { findByText } = render(<RoshamboMatch />);
  const playRound = await findByText(/PLAY ROUND/i);
  expect(playRound).toBeInTheDocument();

  const restartGame = await findByText(/restart game/i);
  expect(restartGame).toBeInTheDocument();

  const totalRounds = await findByText(/Total rounds played/i);
  expect(totalRounds).toBeInTheDocument();

  const playerOne = await findByText(/1st Player chose/i);
  expect(playerOne).toBeInTheDocument();

  const playerTwo = await findByText(/2st Player chose/i);
  expect(playerTwo).toBeInTheDocument();

  const winner = await findByText(/Winner/i);
  expect(winner).toBeInTheDocument();
});

test('find History', async () => {
  const { findByText, findAllByText } = render(<RoshamboMatch />);

  const totalRounds = await findByText(/Total rounds played: 3/i);
  expect(totalRounds).toBeInTheDocument();
  const paper = await findByText(/Paper/i);
  expect(paper).toBeInTheDocument();
  const scissors = await findByText(/Scissors/i);
  expect(scissors).toBeInTheDocument();
  const rock = await findAllByText(/Rock/i);
  expect(rock.length).toBe(4);
  const playerOne = await findByText(/Player 1 win/i);
  expect(playerOne).toBeInTheDocument();
  const playerTwo = await findByText(/Player 2 win/i);
  expect(playerTwo).toBeInTheDocument();
  const draw = await findByText(/Draw/i);
  expect(draw).toBeInTheDocument();
});

test('play match', async () => {
  const { findByText, findAllByText, getByText } = render(<RoshamboMatch />);
  var totalRounds = await findByText(/Total rounds played: 3/i);
  expect(totalRounds).toBeInTheDocument();

  fireEvent.click(getByText(/PLAY ROUND/i));

  totalRounds = await findByText(/Total rounds played: 4/i);
  expect(totalRounds).toBeInTheDocument();
  const paper = await findAllByText(/Paper/i);
  expect(paper.length).toBe(2);
  const rock = await findAllByText(/Rock/i);
  expect(rock.length).toBe(5);
  const playerOne = await findAllByText(/Player 1 win/i);
  expect(playerOne.length).toBe(2);
});

test('reset data', async () => {
  const { findByText, getByText, queryAllByText} = render(<RoshamboMatch />);
  var totalRounds = await findByText(/Total rounds played: 3/i);
  expect(totalRounds).toBeInTheDocument();

  fireEvent.click(getByText(/RESTART GAME/i));

  totalRounds = await findByText(/Total rounds played: 0/i);
  expect(totalRounds).toBeInTheDocument();
  const paper = await queryAllByText(/Paper/i);
  expect(paper.length).toBe(0);
  const rock = await queryAllByText(/Rock/i);
  expect(rock.length).toBe(0);
  const playerOne = await queryAllByText(/Player 1 win/i);
  expect(playerOne.length).toBe(0);
});
