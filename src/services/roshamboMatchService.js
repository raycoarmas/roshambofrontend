import axios from 'axios';
import { ROSHAMBO_URL, ROSHAMBO_PLAY_MATCH_ENDPOINT, ROSHAMBO_HISTORY_ENDPOINT, ROSHAMBO_DELETE_HISTORY_ENDPOINT, ROSHAMBO_TOTAL_MATCHES_WINNERS_ENDPOINT } from '../constants/configConstants';

export function roshamboPlayMatch(setState, state){
    axios.get(`${ROSHAMBO_URL}${ROSHAMBO_PLAY_MATCH_ENDPOINT}`,{withCredentials:true}).then(response =>{
        setState([...state, response.data])
    }).catch(error =>{
        alert("Error playing a roshambo round, please contact an administrator")
    })
}

export function roshamboHistory(setState, state){
    axios.get(`${ROSHAMBO_URL}${ROSHAMBO_HISTORY_ENDPOINT}`,{withCredentials:true}).then(response =>{
        setState([...state, ...response.data]);
    }).catch(error =>{
        alert("Error getting roshambo history")
    })
}

export function roshamboReset(setState, state){
    axios.delete(`${ROSHAMBO_URL}${ROSHAMBO_DELETE_HISTORY_ENDPOINT}`,{withCredentials:true}).then(response =>{
        setState([]);
    }).catch(error =>{
        alert("Error deleting roshambo history")
    })
}

export function roshamboTotals(setState){
    axios.get(`${ROSHAMBO_URL}${ROSHAMBO_TOTAL_MATCHES_WINNERS_ENDPOINT}`,{withCredentials:true}).then(response =>{
        setState(response.data);
    }).catch(error =>{
        alert("Error getting roshambo total winners and matches")
    })
}