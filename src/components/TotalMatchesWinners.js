import React, { useState, useEffect } from 'react'
import { Button, Grid, Typography } from '@material-ui/core'
import { useHistory } from 'react-router-dom'
import { roshamboTotals } from '../services/roshamboMatchService'
import styles from './../styles/TotalMatchesWinners.module.css';


const initialState = {
    data: {
        playerOneWins: 0,
        playerTwoWins: 0,
        draws: 0,
        totalMatches: 0
    },
    load: false
}

function TotalMatchesWinners() {
    const [data, setData] = useState(initialState.data)
    const [load, setLoad] = useState(initialState.load)
    const history = useHistory();

    useEffect(() => {
        if (!load) {
            roshamboTotals(setData, data)
            setLoad(true)
        }
    }, [load, data])

    return (
        <Grid className={styles.gridContainer} container spacing={8}>
            <Grid className={styles.itemContainer} item xs={12}>
                <Typography variant="h6">Total rounds played: {data.totalMatches}</Typography>
            </Grid>
            <Grid className={styles.itemContainer} item xs={12}>

                <Grid container spacing={10} direction="row" alignItems="center">

                    <Typography className={styles.typography} variant="h6">Player one: <br/>{data.playerOneWins} wins</Typography>
                    <Typography className={styles.typography} variant="h6">Player two: <br/>{data.playerTwoWins} wins</Typography>
                    <Typography className={styles.typography} variant="h6">Draws: <br/>{data.draws}</Typography>
                </Grid>
            </Grid>


            <Grid className={styles.itemContainer} item xs={12}>
                <Button color="primary" onClick={() => history.goBack("/totals")}>Back</Button>
            </Grid>
        </Grid>
    )
}

export default TotalMatchesWinners