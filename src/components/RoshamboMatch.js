import React, { useState, useEffect } from 'react';
import { Button, Grid } from '@material-ui/core';
import MaterialTable from 'material-table';
import { useHistory } from 'react-router-dom';
import styles from './../styles/RoshamboMatch.module.css';
import { roshamboPlayMatch, roshamboHistory, roshamboReset } from '../services/roshamboMatchService';

const initialState = {
  data: [],
  load: false
}

function RoshamboMatch() {
  const [data, setData] = useState(initialState.data)
  const [load, setLoad] = useState(initialState.load)
  const history = useHistory();

  useEffect(() => {
    if (!load) {
      roshamboHistory(setData, data)
      setLoad(true)
    }
  }, [load, data])

  return (
    <Grid className={styles.gridContainer} container spacing={3}>
      <Grid className={styles.itemContainer} item xs={12}>
        <MaterialTable style={{width: "75%", justifySelf: "center"}}
          columns={[
            { title: '1st Player chose', field: 'playerOneChoose' },
            { title: '2st Player chose', field: 'playerTwoChoose' },
            { title: 'Winner', field: 'playerWinner' }
          ]}
          data={data}
          title={`Total rounds played: ${data.length}`}
          options={{ search: false }}
        />
      </Grid>
      <Grid className={styles.itemContainer} item xs={4} >
        <Button className={styles.buttonRestart} variant="outlined" color="primary" onClick={() => roshamboReset(setData)}>Restart game</Button>
      </Grid>
      <Grid  className={styles.itemContainer} item xs={4}>
        <Button className={styles.buttonPlay} variant="contained" color="primary" onClick={() => roshamboPlayMatch(setData, data)}>Play round</Button>
      </Grid>
      <Grid  className={styles.itemContainer} item xs={4}>
        <Button className={styles.buttonPlay} color="primary" onClick={() => history.push("/totals")}>Show totals</Button>
      </Grid>
    </Grid>
  );
}

export default RoshamboMatch;
