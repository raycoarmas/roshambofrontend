import React from 'react'
import { Switch, Route, BrowserRouter } from 'react-router-dom'
import { StylesProvider } from '@material-ui/core/styles';
import RoshamboMatch from './components/RoshamboMatch'
import TotalMatchesWinners from './components/TotalMatchesWinners';

function Routes() {
    return (
        <StylesProvider injectFirst>
            <BrowserRouter >
                <Switch>
                    <Route exact path="/" component={RoshamboMatch} />
                    <Route path="/totals" component={TotalMatchesWinners}/>
                </Switch>
            </BrowserRouter >
        </StylesProvider>
    )
}

export default Routes
